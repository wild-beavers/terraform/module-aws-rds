#####
# Variables
#####

variable "db_subnet_group_name" {
  description = "Name of existing database DB Subnet Group. Conflicts `var.db_subnet_group`."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.db_subnet_group_name == "" || can(regex("^[a-zA-Z_0-9\\-\\.\\ ]{1,255}$", var.db_subnet_group_name))
    error_message = "“var.db_subnet_group_name” is invalid."
  }
  validation {
    condition     = var.db_subnet_group == null || var.db_subnet_group_name == ""
    error_message = "“var.db_subnet_group_name” conflicts with “db_subnet_group”."
  }
}

variable "db_subnet_group" {
  description = <<-DOCUMENTATION
Parameter Group to create.
  - name         (required, string):             Name of the DB Subnet Group.
  - subnet_ids   (required, list(string), []):   VPC Subnet IDs to be used to create a new DB Subnet Group. If provided, a new DB Subnet Group will be created, otherwise an existing Subnet Group `var.db_subnet_group_name` will be used instead.
  - description  (optional, string):             Description of the DB Subnet Group.
  - tags         (optional, map(string), {}):    Tags to assign to the resource, will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name        = string
    subnet_ids  = list(string)
    description = optional(string, "")
    tags        = optional(map(string), {})
  })
  default = null

  validation {
    condition     = var.db_subnet_group == null || can(regex("^[a-zA-Z_0-9\\-\\.\\ ]{1,255}$", try(var.db_subnet_group.name, [])))
    error_message = "“var.db_subnet_group.name” is invalid."
  }
  validation {
    condition = var.db_subnet_group == null || alltrue([for subnet in try(var.db_subnet_group.subnet_ids, []) :
      can(regex("^subnet-[a-zA-Z_0-9]{17}$", subnet))
    ])
    error_message = "One ore more “var.db_subnet_group.subnet_ids” are invalid."
  }
}

#####
# Resources
#####

locals {
  db_subnet_group_name = var.db_subnet_group_name != "" ? var.db_subnet_group_name : (var.db_subnet_group != null ? aws_db_subnet_group.this["0"].name : null)
}

resource "aws_db_subnet_group" "this" {
  for_each = var.db_subnet_group != null ? { 0 = "enabled" } : {}

  name        = "${var.testing_prefix}${var.db_subnet_group.name}"
  description = var.db_subnet_group.description == "" ? local.default_description : var.db_subnet_group.description
  subnet_ids  = var.db_subnet_group.subnet_ids
  tags = merge(
    local.tags,
    var.db_subnet_group.tags,
    {
      Name        = "${var.testing_prefix}${var.db_subnet_group.name}"
      Description = var.db_subnet_group.description == "" ? local.default_description : var.db_subnet_group.description
    },
  )
}

#####
# Outputs
#####

output "aws_db_subnet_group" {
  value = var.db_subnet_group != null ? { for k, v in aws_db_subnet_group.this["0"] :
    k => v if !contains(["name_prefix", "tags", "subnet_ids"], k)
  } : null
}
