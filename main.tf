locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-rds"
    }
  )
  default_description = format("For %s %s%s", var.database.cluster ? "RDS cluster" : "DB instance", var.testing_prefix, var.database.identifier != null ? var.database.identifier : "")
}

resource "random_id" "final_snapshot" {
  for_each = var.database.final_snapshot_identifier != null ? { 0 = 0 } : {}

  # To create this list, please select all parameters with the flag ForceNew on those pages :
  # https://github.com/hashicorp/terraform-provider-aws/blob/main/internal/service/rds/instance.go
  # https://github.com/hashicorp/terraform-provider-aws/blob/main/internal/service/rds/cluster_instance.go
  keepers = {
    availability_zones       = join(",", var.database.availability_zones)
    backup_target            = var.database.backup_target
    character_set_name       = var.database.character_set_name
    db_name                  = var.database.db_name
    db_subnet_group_name     = var.db_subnet_group != null ? var.db_subnet_group.name : var.db_subnet_group_name
    engine                   = var.database.engine
    engine_mode              = var.database.engine_mode
    identifier               = var.database.identifier
    instance_profile         = var.database.custom_iam_instance_profile
    kms_key_id               = local.kms_key_id
    master_username          = var.database.username
    nchar_character_set_name = var.database.nchar_character_set_name
    port                     = var.database.port
    s3_import                = jsonencode(var.database.s3_import)
    snapshot_identifier      = var.database.snapshot_identifier
  }

  byte_length = 5
}

#####
# DB instance
#####

variable "database" {
  description = <<-DOCUMENTATION
Options of the the database.
Depending on `cluster`, option may or may not be relevant, so be careful when using them.
A non-relevant option will likely be silent.
Shared options (between cluster and single DB) are first, then cluster, then single DB.

- cluster (optional, bool, false): Makes database HA using a cluster.
- size    (optional, number, 0):   Size of the cluster. Only useful for Aurora-based clusters.

- allocated_storage  (required, number):  Amount of storage in gibibytes (GiB) to allocate to DB instance(s).
- engine             (required, string):  Name of the database engine to be used for this DB cluster or DB instance.
- instance_class     (required, string):  Instance type of the RDS instance.

- allow_major_version_upgrade           (optional, bool, false):        Indicates that major version upgrades are allowed.
- apply_immediately                     (optional, bool, false):        Whether any database modifications are applied immediately, or during the next maintenance window.
- auto_minor_version_upgrade            (optional, bool, true):         Indicates that minor engine upgrades will be applied automatically to the DB instance(s) during the maintenance window.
- availability_zones                    (optional, list(string)):       List of EC2 Availability Zones for the DB cluster storage where DB cluster instances can be created. RDS automatically assigns 3 AZs if less than 3 AZs are configured, which will show as a difference requiring resource recreation next Terraform apply. For a single DB instance, only the first AZ of the list will be used.
- backup_retention_period               (optional, number, 1):          The days to retain backups for. Must be between 0 and 35.
- backup_window                         (optional, string):             The daily time range (in UTC) during which automated backups are created if they are enabled. Example: "09:46-10:16". Must not overlap with `maintenance_window`.
- ca_cert_identifier                    (optional, string):             Identifier of the CA certificate for the DB instance(s).
- copy_tags_to_snapshot                 (optional, bool, false):        Copy all DB instance(s) tags to snapshots.
- cluster_identifier                    (optional, string):             Cluster identifier.
- custom_iam_instance_profile           (optional, string):             Instance profile associated with the underlying Amazon EC2 instance of an RDS Custom DB instance.
- db_name                               (optional, string):             Name of the database to create when the DB instance(s) is/are created. If this parameter is not specified, no database is created in the DB instance(s).
- delete_automated_bsackups             (optional, bool, true):         Whether to remove automated backups immediately after the DB instance(s) is/are deleted.
- deletion_protection                   (optional, bool, false):        Whether to protect DB instance(s) against accidental deletions. The database can’t be deleted when this value is set to `true`.
- domain                                (optional, string):             ID of the Directory Service Active Directory domain to create the instance(s) in. Conflicts with `domain_fqdn`, `domain_ou`, `domain_auth_secret_arn` and a `domain_dns_ips`.
- domain_iam_role_name                  (optional, string):             Name of the IAM role to be used when making API calls to the Directory Service
- enabled_cloudwatch_logs_exports       (optional, set(string)):        Set of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. The following log types are supported: `audit`, `error`, `general`, `slowquery`, `postgresql`.
- engine_version                        (optional, string):             Engine version to use. If `auto_minor_version_upgrade` is enabled, you can provide a prefix of the version such as "8.0" (for "8.0.36").
- engine_lifecycle_support              (optional, string):             Life cycle type for DB instance(s). This setting applies only to MySQL or PostgreSQL and their Aurora variants. Valid values are `open-source-rds-extended-support`, `open-source-rds-extended-support-disabled`. Default value is `open-source-rds-extended-support`.
- final_snapshot_identifier             (optional, string):             Name of the final DB snapshot when DB instance(s) is/are deleted. If omitted, no final snapshot will be made.
- iam_database_authentication_enabled   (optional, bool, false):        Whether mappings of AWS IAM accounts to database accounts is enabled.
- identifier                            (optional, string):             Name of the RDS instance (or cluster), if omitted, Terraform will assign a random, unique identifier.
- iops                                  (optional, number):             Amount of provisioned IOPS. Setting this implies a storage_type of "io1" or "io2". Can only be set when storage_type is "io1", "io2 or "gp3". Cannot be specified for gp3 storage if the allocated_storage value is below a per-engine threshold.
- maintenance_window                    (optional, string):             Weekly time range during which system maintenance can occur, in (UTC) e.g., wed:04:00-wed:04:30
- manage_master_user_password           (optional, bool, false):        Allow RDS to manage the master user password in Secrets Manager. Cannot be set if `password` is provided.
- username                              (optional, string):             Username for the master DB user.
- password                              (optional, string):             Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file.
- monitoring_interval                   (optional, number):             Interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60.
- monitoring_role_arn                   (optional, string):             ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs.
- network_type                          (optional, string):             Network type of the DB instance. Valid values: IPV4, DUAL.
- performance_insights_enabled          (optional, bool, false):        Whether Performance Insights are enabled.
- performance_insights_retention_period (optional, number):             Amount of time in days to retain Performance Insights data. Valid values are 7, 731 (2 years) or a multiple of 31. When specifying `performance_insights_retention_period`, `performance_insights_enabled` needs to be set to true.
- port                                  (optional, number):             Port on which the DB accepts connections.
- publicly_accessible                   (optional, bool, false):        Control if instance is publicly accessible.
- restore_to_point_in_time              (optional, object):
   - source_cluster_identifier                (optional, string):      Identifier of the source database cluster from which to restore. When restoring from a cluster in another AWS account, the identifier is the ARN of that cluster.
   - source_cluster_resource_id               (optional, string):      Cluster resource ID of the source database cluster from which to restore. To be used for restoring a deleted cluster in the same account which still has a retained automatic backup available.
   - restore_type                             (optional, string):      Type of restore to be performed. Valid options are full-copy (default) and copy-on-write.
   - use_latest_restorable_time               (optional, bool, false): Whether to restore the database cluster to the latest restorable backup time. Defaults to false. Conflicts with `restore_to_time`.
   - source_db_instance_identifier            (optional, string):      Identifier of the source DB instance from which to restore. Must match the identifier of an existing DB instance. Required if source_db_instance_automated_backups_arn or source_dbi_resource_id is not specified.
   - source_db_instance_automated_backups_arn (optional, string):      ARN of the automated backup from which to restore. Required if source_db_instance_identifier or source_dbi_resource_id is not specified.
   - restore_time                             (optional, string):      Date and time in UTC format to restore the database cluster to. Conflicts with `use_latest_restorable_time`.
- skip_final_snapshot                   (optional, bool, false):        Whether a final DB snapshot is created before the DB instance is deleted. If "true", no DBSnapshot is created. If "false", a DB snapshot is created before the DB instance is deleted, using the value from `final_snapshot_identifier`. Enabling this option also allows for easy engine updates, as final snapshots can be used to restore the database automatically.
- snapshot_identifier                   (optional, string):             Whether to create this database from a snapshot. This correlates to the snapshot ID you’d find in the RDS console, e.g: rds:production-2015-06-26-06-05.
- storage_encrypted                     (optional, bool):               Whether the DB instance is encrypted. Note that if you are creating a cross-region read replica this field is ignored and you should instead declare `var.kms_keys.db` or `var.kms_key_id`.
- storage_type                          (optional, string):             One of "standard" (magnetic), "gp2" (general purpose SSD), "gp3" (general purpose SSD that needs iops independently) "io1" (provisioned IOPS SSD) or "io2" (block express storage provisioned IOPS SSD). The default is "io1" if iops is specified, "gp2" if not.
- tags                                  (optional, map(string)):        Map of tags to assign to the database. Will be merged with `var.tags`.

- domain_ou                 (optional, string):       Self managed Active Directory organizational unit for your DB instance to join. Conflicts with domain and domain_iam_role_name.
- dedicated_log_volume      (optional, bool):         Use a dedicated log volume (DLV) for the DB instance. Requires Provisioned IOPS.
- character_set_name        (optional, string):       The character set name to use for DB encoding in Oracle and Microsoft SQL instances (collation). This can’t be changed.
- backup_target             (optional, string):       Specifies where automated backups and manual snapshots are stored. Possible values are region (default) and outposts.
- blue_green_update         (optional, bool, false):  Enables low-downtime updates using RDS Blue/Green deployments.
- domain_auth_secret_arn    (optional, string):       ARN for the Secrets Manager secret with the self managed Active Directory credentials for the user joining the domain. Conflicts with `domain` and `domain_iam_role_name`.
- domain_dns_ips            (optional, list(string)): The IPv4 DNS IP addresses of your primary and secondary self managed Active Directory domain controllers. Two IP addresses must be provided. If there isn’t a secondary domain controller, use the IP address of the primary domain controller for both entries in the list. Conflicts with `domain` and `domain_iam_role_name`.
- domain_fqdn               (optional, string):       Fully qualified domain name (FQDN) of the self managed Active Directory domain. Conflicts with `domain` and `domain_iam_role_name`.
- license_model             (optional, string):       License model information for this DB instance.
- max_allocated_storage     (optional, number):       When configured, the upper limit to which Amazon RDS can automatically scale the storage of the DB instance. Configuring this will automatically ignore differences to allocated_storage. Must be greater than or equal to allocated_storage or 0 to disable Storage Autoscaling.
- multi_az                  (optional, bool, false):  Specifies if the RDS instance is multi-AZ.
- nchar_character_set_name  (optional, string):       National character set is used in the NCHAR, NVARCHAR2, and NCLOB data types for Oracle instances. This can’t be changed.
- replica_mode              (optional, bool):         Whether the replica is in either mounted or open-read-only mode. This attribute is only supported by Oracle instances. Oracle replicas operate in open-read-only mode unless otherwise specified.
- replicate_source_db       (optional, string):       Specifies that this resource is a Replicate database, and to use this value as the source database. This correlates to the identifier of another Amazon RDS Database to replicate (if replicating within a single region) or ARN of the Amazon RDS Database to replicate (if replicating cross-region). Note that if you are creating a cross-region replica of an encrypted database you will also need to specify a KMS key.
- upgrade_storage_config    (optional, bool):         Whether to upgrade the storage file system configuration on the read replica. Can only be set with replicate_source_db.
- s3_import                 (optional, object):       Restore from a Percona Xtrabackup in S3:
   - bucket_name           (required, string): Bucket name where the backup is stored
   - ingestion_role        (required, string): Role applied to load the data.
   - source_engine         (required, string): Source engine for the backup
   - source_engine_version (required, string): Version of the source engine used to make the backup
   - bucket_prefix         (optional, string): Can be blank, but is the path to your backup
- storage_throughput        (optional, string):       Storage throughput value for the DB instance. Can only be set when storage_type is "gp3". Cannot be specified if the allocated_storage value is below a per-engine threshold.
- timezone                  (optional, string):       Time zone of the DB instance. timezone is currently only supported by Microsoft SQL Server. The timezone can only be set on creation.
- customer_owned_ip_enabled (optional, bool):         Whether to enable a customer-owned IP address (CoIP) for an RDS on Outposts DB instance.

- db_system_id                  (optional, string):       For use with RDS Custom.
- backtrack_window              (optional, number):       Target backtrack window, in seconds. Only available for aurora and aurora-mysql engines currently. To disable backtracking, set this value to 0. Defaults to 0. Must be between 0 and 259200 (72 hours)
- enable_http_endpoint          (optional, string):       Enable HTTP endpoint (data API). Only valid for some combinations of engine_mode, engine and engine_version and only available in some regions. This option also does not work with any of these options specified: snapshot_identifier, replication_source_identifier, s3_import.
- enable_local_write_forwarding (optional, bool):         Whether read replicas can forward write operations to the writer DB instance in the DB cluster. By default, write operations aren’t allowed on reader DB instances. NOTE: Local write forwarding requires Aurora MySQL version 3.04 or higher.
- engine_mode                   (optional, string):       Database engine mode. Valid values: global (only valid for Aurora MySQL 1.21 and earlier), parallelquery, provisioned, serverless. Defaults to: provisioned.
- iam_roles                     (optional, list(string)): List of ARNs for the IAM roles to associate to the RDS Cluster.
- replication_source_identifier (optional, string):       ARN of a source DB cluster or DB instance if this DB cluster is to be created as a Read Replica. If DB Cluster is part of a Global Cluster, use the lifecycle configuration block ignore_changes argument to prevent Terraform from showing differences for this argument instead of configuring this value.
- promotion_tier                (optional, number):       Failover Priority setting on instance level. The reader who has lower tier has higher priority to get promoted to writer.
- scaling_configuration         (optional, object):       Nested attribute with scaling properties. Only valid when `engine_mode` is set to `serverless`. More details below.
   - auto_pause               (optional, bool, true): Whether to enable automatic pause. A DB cluster can be paused only when it’s idle (it has no connections). If a DB cluster is paused for more than seven days, the DB cluster might be backed up with a snapshot. In this case, the DB cluster is restored when there is a request to connect to it.
   - max_capacity             (optional, number):     Maximum capacity for an Aurora DB cluster in serverless DB engine mode. The maximum capacity must be greater than or equal to the minimum capacity. Valid Aurora MySQL capacity values are 1, 2, 4, 8, 16, 32, 64, 128, 256. Valid Aurora PostgreSQL capacity values are (2, 4, 8, 16, 32, 64, 192, and 384). Defaults to 16.
   - min_capacity             (optional, number):     Minimum capacity for an Aurora DB cluster in serverless DB engine mode. The minimum capacity must be lesser than or equal to the maximum capacity. Valid Aurora MySQL capacity values are 1, 2, 4, 8, 16, 32, 64, 128, 256. Valid Aurora PostgreSQL capacity values are (2, 4, 8, 16, 32, 64, 192, and 384). Defaults to 1.
   - seconds_before_timeout   (optional, number):     Amount of time, in seconds, that Aurora Serverless v1 tries to find a scaling point to perform seamless scaling before enforcing the timeout action. Valid values are 60 through 600. Defaults to 300.
   - seconds_until_auto_pause (optional, number):     Time, in seconds, before an Aurora DB cluster in serverless mode is paused. Valid values are 300 through 86400. Defaults to 300.
   - timeout_action           (optional, string):     Action to take when the timeout is reached. Valid values: ForceApplyCapacityChange, RollbackCapacityChange. Defaults to RollbackCapacityChange.
- serverlessv2_scaling_configuration (optional, object): Nested attribute with scaling properties for ServerlessV2. Only valid when engine_mode is set to provisioned. More details below.
   - max_capacity (required, number): Maximum capacity for an Aurora DB cluster in provisioned DB engine mode. The maximum capacity must be greater than or equal to the minimum capacity. Valid capacity values are in a range of 0.5 up to 128 in steps of 0.5.
   - min_capacity (required, number): Minimum capacity for an Aurora DB cluster in provisioned DB engine mode. The minimum capacity must be lesser than or equal to the maximum capacity. Valid capacity values are in a range of 0.5 up to 128 in steps of 0.5.
- source_region (optional, string): The source region for an encrypted replica DB cluster.
DOCUMENTATION
  type = object({
    cluster = optional(bool, false)
    size    = optional(number, 2)

    allocated_storage = number
    engine            = string
    instance_class    = string

    allow_major_version_upgrade           = optional(bool, false)
    apply_immediately                     = optional(bool, false)
    auto_minor_version_upgrade            = optional(bool, true)
    availability_zones                    = optional(list(string), [])
    backup_retention_period               = optional(number, 1)
    ca_cert_identifier                    = optional(string)
    cluster_identifier                    = optional(string)
    copy_tags_to_snapshot                 = optional(bool, false)
    custom_iam_instance_profile           = optional(string)
    db_name                               = optional(string)
    delete_automated_backups              = optional(bool, true)
    deletion_protection                   = optional(bool, false)
    domain                                = optional(string)
    domain_iam_role_name                  = optional(string)
    enabled_cloudwatch_logs_exports       = optional(set(string))
    engine_version                        = optional(string)
    engine_lifecycle_support              = optional(string)
    final_snapshot_identifier             = optional(string)
    iam_database_authentication_enabled   = optional(bool, false)
    identifier                            = optional(string)
    iops                                  = optional(number)
    maintenance_window                    = optional(string)
    manage_master_user_password           = optional(bool, false)
    username                              = optional(string)
    password                              = optional(string)
    monitoring_interval                   = optional(number)
    monitoring_role_arn                   = optional(string)
    network_type                          = optional(string)
    performance_insights_enabled          = optional(bool, false)
    performance_insights_retention_period = optional(number)
    port                                  = optional(number)
    publicly_accessible                   = optional(bool, false)
    restore_to_point_in_time = optional(object({
      source_cluster_resource_id               = optional(string)
      source_cluster_identifier                = optional(string)
      restore_type                             = optional(string)
      use_latest_restorable_time               = optional(bool, false)
      source_db_instance_identifier            = optional(string)
      source_db_instance_automated_backups_arn = optional(string)
      restore_time                             = optional(string)
    }))
    skip_final_snapshot = optional(bool, false)
    snapshot_identifier = optional(string)
    storage_encrypted   = optional(bool)
    storage_type        = optional(string)
    tags                = optional(map(string), {})

    domain_ou                = optional(string)
    dedicated_log_volume     = optional(bool)
    character_set_name       = optional(string)
    backup_target            = optional(string)
    backup_window            = optional(string)
    blue_green_update        = optional(bool, false)
    domain_auth_secret_arn   = optional(string)
    domain_dns_ips           = optional(list(string))
    domain_fqdn              = optional(string)
    license_model            = optional(string)
    max_allocated_storage    = optional(number)
    multi_az                 = optional(bool, false)
    nchar_character_set_name = optional(string)
    replica_mode             = optional(bool)
    replicate_source_db      = optional(string)
    upgrade_storage_config   = optional(bool)
    s3_import = optional(object({
      bucket_name           = string
      ingestion_role        = string
      source_engine         = string
      source_engine_version = string
      bucket_prefix         = optional(string)
    }))
    storage_throughput        = optional(string)
    timezone                  = optional(string)
    customer_owned_ip_enabled = optional(bool)

    db_system_id                  = optional(string)
    backtrack_window              = optional(number)
    enable_http_endpoint          = optional(string)
    enable_local_write_forwarding = optional(bool)
    engine_mode                   = optional(string)
    iam_roles                     = optional(list(string))
    replication_source_identifier = optional(string)
    promotion_tier                = optional(number)
    scaling_configuration = optional(object({
      auto_pause               = optional(bool, true)
      max_capacity             = optional(number)
      min_capacity             = optional(number)
      seconds_before_timeout   = optional(number)
      seconds_until_auto_pause = optional(number)
      timeout_action           = optional(string)
    }))
    serverlessv2_scaling_configuration = optional(object({
      max_capacity = number
      min_capacity = number
    }))
    source_region = optional(string)
  })
  default = null
}

resource "aws_db_instance" "this" {
  for_each = !var.database.cluster ? { 0 = "enabled" } : {}

  allocated_storage = var.database.allocated_storage
  engine            = var.database.engine
  instance_class    = var.database.instance_class

  allow_major_version_upgrade           = var.database.allow_major_version_upgrade
  apply_immediately                     = var.database.apply_immediately
  auto_minor_version_upgrade            = var.database.auto_minor_version_upgrade
  availability_zone                     = length(var.database.availability_zones) > 0 ? var.database.availability_zones[0] : null
  backup_retention_period               = var.database.backup_retention_period
  backup_target                         = var.database.backup_target
  backup_window                         = var.database.backup_window
  ca_cert_identifier                    = var.database.ca_cert_identifier
  character_set_name                    = var.database.character_set_name
  copy_tags_to_snapshot                 = var.database.copy_tags_to_snapshot
  custom_iam_instance_profile           = var.database.custom_iam_instance_profile
  customer_owned_ip_enabled             = var.database.customer_owned_ip_enabled
  db_name                               = var.database.db_name
  db_subnet_group_name                  = local.db_subnet_group_name
  dedicated_log_volume                  = var.database.dedicated_log_volume
  delete_automated_backups              = var.database.delete_automated_backups
  deletion_protection                   = var.database.deletion_protection
  domain                                = var.database.domain
  domain_auth_secret_arn                = var.database.domain_auth_secret_arn
  domain_dns_ips                        = var.database.domain_dns_ips
  domain_fqdn                           = var.database.domain_fqdn
  domain_iam_role_name                  = var.database.domain_iam_role_name != null ? "${var.testing_prefix}${var.database.domain_iam_role_name}" : null
  domain_ou                             = var.database.domain_ou
  enabled_cloudwatch_logs_exports       = var.database.enabled_cloudwatch_logs_exports
  engine_lifecycle_support              = var.database.engine_lifecycle_support
  engine_version                        = var.database.engine_version
  final_snapshot_identifier             = var.database.final_snapshot_identifier != null ? format("%s%s-%s", var.testing_prefix, var.database.final_snapshot_identifier, random_id.final_snapshot["0"].dec) : null
  iam_database_authentication_enabled   = var.database.iam_database_authentication_enabled
  identifier                            = var.database.identifier != null ? "${var.testing_prefix}${var.database.identifier}" : null
  iops                                  = var.database.iops
  kms_key_id                            = local.kms_key_id
  license_model                         = var.database.license_model
  maintenance_window                    = var.database.maintenance_window
  manage_master_user_password           = !var.database.manage_master_user_password ? null : true
  master_user_secret_kms_key_id         = var.database.manage_master_user_password ? local.kms_key_id : null
  max_allocated_storage                 = var.database.max_allocated_storage
  monitoring_interval                   = var.database.monitoring_interval
  monitoring_role_arn                   = var.database.monitoring_role_arn
  multi_az                              = var.database.multi_az
  nchar_character_set_name              = var.database.nchar_character_set_name
  network_type                          = var.database.network_type
  parameter_group_name                  = local.db_parameter_group_needed ? aws_db_parameter_group.this["0"].name : null
  password                              = var.secret != null && !try(var.secret.use_provided_password, false) ? jsondecode(data.aws_secretsmanager_secret_version.this["0"].secret_string)[var.secret.password_key] : var.database.password
  performance_insights_enabled          = var.database.performance_insights_enabled
  performance_insights_kms_key_id       = local.kms_performance_insight_key_id
  performance_insights_retention_period = var.database.performance_insights_retention_period
  port                                  = var.database.port
  publicly_accessible                   = var.database.publicly_accessible
  replica_mode                          = var.database.replica_mode
  replicate_source_db                   = var.database.replicate_source_db
  skip_final_snapshot                   = var.database.skip_final_snapshot
  snapshot_identifier                   = var.database.snapshot_identifier
  storage_encrypted                     = var.database.storage_encrypted
  storage_throughput                    = var.database.storage_throughput
  storage_type                          = var.database.storage_type
  tags                                  = merge(local.tags, var.database.tags)
  timezone                              = var.database.timezone
  upgrade_storage_config                = var.database.upgrade_storage_config
  username                              = var.database.username
  vpc_security_group_ids                = concat(var.security_group_ids, var.security_group != null ? [aws_security_group.this["0"].id] : [])

  dynamic "blue_green_update" {
    for_each = var.database.blue_green_update ? [1] : []

    content {
      enabled = true
    }
  }

  dynamic "restore_to_point_in_time" {
    for_each = var.database.restore_to_point_in_time != null ? [1] : []

    content {
      source_db_instance_identifier            = var.database.restore_to_point_in_time.source_db_instance_identifier
      source_db_instance_automated_backups_arn = var.database.restore_to_point_in_time.source_db_instance_automated_backups_arn
      use_latest_restorable_time               = var.database.restore_to_point_in_time.use_latest_restorable_time
      restore_time                             = var.database.restore_to_point_in_time.restore_time
    }
  }

  dynamic "s3_import" {
    for_each = var.database.s3_import != null ? [1] : []

    content {
      bucket_name           = var.database.s3_import.bucket_name
      ingestion_role        = var.database.s3_import.ingestion_role
      source_engine         = var.database.s3_import.source_engine
      source_engine_version = var.database.s3_import.source_engine_version
      bucket_prefix         = var.database.s3_import.bucket_prefix
    }
  }

  lifecycle {
    # Required to avoid idempotency issues when using secrets manager.
    # Anyway, It is recommended to either use secrets manager or update the password manually after first deployment,
    # to avoid having a cleartext password in the Terraform state file.
    ignore_changes = [password]
  }
}

output "aws_db_instance" {
  value = !var.database.cluster ? { for k, v in aws_db_instance.this["0"] :
    k => v if !contains(["tags", "password", "identifier_prefix", "domain_dns_ips", "enabled_cloudwatch_logs_exports", "latest_restorable_time", "master_password_wo", "password_wo"], k)
  } : null
}

resource "aws_rds_cluster" "this" {
  for_each = var.database.cluster ? { 0 = "enabled" } : {}

  allocated_storage         = var.database.allocated_storage
  engine                    = var.database.engine
  db_cluster_instance_class = var.database.instance_class

  db_cluster_parameter_group_name = local.db_parameter_group_needed ? aws_db_parameter_group.this["0"].name : null
  kms_key_id                      = local.kms_key_id
  master_user_secret_kms_key_id   = var.database.manage_master_user_password ? local.kms_key_id : null
  db_subnet_group_name            = local.db_option_group_needed ? aws_db_subnet_group.this["0"].name : null
  vpc_security_group_ids          = concat(var.security_group_ids, var.security_group != null ? [aws_security_group.this["0"].id] : [])
  performance_insights_kms_key_id = local.kms_performance_insight_key_id

  allow_major_version_upgrade           = var.database.allow_major_version_upgrade
  apply_immediately                     = var.database.apply_immediately
  availability_zones                    = var.database.availability_zones
  backup_retention_period               = var.database.backup_retention_period
  copy_tags_to_snapshot                 = var.database.copy_tags_to_snapshot
  delete_automated_backups              = var.database.delete_automated_backups
  deletion_protection                   = var.database.deletion_protection
  domain                                = var.database.domain
  domain_iam_role_name                  = var.database.domain_iam_role_name
  enabled_cloudwatch_logs_exports       = var.database.enabled_cloudwatch_logs_exports
  engine_version                        = var.database.engine_version
  engine_lifecycle_support              = var.database.engine_lifecycle_support
  final_snapshot_identifier             = var.database.final_snapshot_identifier != null ? format("%s%s-%s", var.testing_prefix, var.database.final_snapshot_identifier, random_id.final_snapshot["0"].dec) : null
  iam_database_authentication_enabled   = var.database.iam_database_authentication_enabled
  cluster_identifier                    = var.database.cluster_identifier != null ? "${var.testing_prefix}${var.database.cluster_identifier}" : null
  iops                                  = var.database.iops
  preferred_maintenance_window          = var.database.maintenance_window
  manage_master_user_password           = !var.database.manage_master_user_password ? null : true
  master_username                       = var.database.username
  master_password                       = var.secret != null && !try(var.secret.use_provided_password, false) ? jsondecode(data.aws_secretsmanager_secret_version.this["0"].secret_string)[var.secret.password_key] : var.database.password
  network_type                          = var.database.network_type
  performance_insights_enabled          = var.database.performance_insights_enabled
  performance_insights_retention_period = var.database.performance_insights_retention_period
  port                                  = var.database.port
  skip_final_snapshot                   = var.database.skip_final_snapshot
  snapshot_identifier                   = var.database.snapshot_identifier
  storage_encrypted                     = var.database.storage_encrypted
  storage_type                          = var.database.storage_type
  tags                                  = merge(local.tags, var.database.tags)
  db_system_id                          = var.database.db_system_id
  backtrack_window                      = var.database.backtrack_window
  enable_http_endpoint                  = var.database.enable_http_endpoint
  enable_local_write_forwarding         = var.database.enable_local_write_forwarding
  engine_mode                           = var.database.engine_mode
  iam_roles                             = var.database.iam_roles
  replication_source_identifier         = var.database.replication_source_identifier

  dynamic "serverlessv2_scaling_configuration" {
    for_each = var.database.serverlessv2_scaling_configuration != null ? [1] : []

    content {
      max_capacity = var.database.serverlessv2_scaling_configuration.max_capacity
      min_capacity = var.database.serverlessv2_scaling_configuration.min_capacity
    }
  }

  dynamic "scaling_configuration" {
    for_each = var.database.scaling_configuration != null ? [1] : []

    content {
      auto_pause               = var.database.scaling_configurationauto_pause
      max_capacity             = var.database.scaling_configurationmax_capacity
      min_capacity             = var.database.scaling_configurationmin_capacity
      seconds_before_timeout   = var.database.scaling_configurationseconds_before_timeout
      seconds_until_auto_pause = var.database.scaling_configurationseconds_until_auto_pause
      timeout_action           = var.database.scaling_configurationtimeout_action
    }
  }

  dynamic "restore_to_point_in_time" {
    for_each = var.database.restore_to_point_in_time != null ? [1] : []

    content {
      source_cluster_resource_id = var.database.restore_to_point_in_time.source_cluster_resource_id
      source_cluster_identifier  = var.database.restore_to_point_in_time.source_cluster_identifier
      restore_type               = var.database.restore_to_point_in_time.restore_type
      use_latest_restorable_time = var.database.restore_to_point_in_time.use_latest_restorable_time
      restore_to_time            = var.database.restore_to_point_in_time.restore_time
    }
  }

  lifecycle {
    # master_password: Required to avoid idempotency issues when using secrets manager.
    #   Anyway, It is recommended to either use secrets manager or update the password manually after first deployment,
    #   to avoid having a cleartext password in the Terraform state file.
    # availability_zones: it seems even if 3 AZs are given, terraform still tries to re-create cluster.
    # iops: fails idempotency
    ignore_changes = [master_password, availability_zones, iops]
  }
}

resource "aws_rds_cluster_instance" "this" {
  count = var.database.cluster ? var.database.size : 0

  apply_immediately                     = var.database.apply_immediately
  auto_minor_version_upgrade            = var.database.auto_minor_version_upgrade
  availability_zone                     = var.database.availability_zones[count.index]
  ca_cert_identifier                    = var.database.ca_cert_identifier
  cluster_identifier                    = aws_rds_cluster.this["0"].id
  copy_tags_to_snapshot                 = var.database.copy_tags_to_snapshot
  custom_iam_instance_profile           = var.database.custom_iam_instance_profile
  db_parameter_group_name               = local.db_parameter_group_needed ? aws_db_parameter_group.this["0"].name : null
  db_subnet_group_name                  = local.db_subnet_group_name
  engine                                = aws_rds_cluster.this["0"].engine
  engine_version                        = aws_rds_cluster.this["0"].engine_version
  identifier                            = var.database.identifier != null ? "${var.testing_prefix}${var.database.identifier}" : null
  instance_class                        = var.database.instance_class
  monitoring_interval                   = var.database.monitoring_interval
  monitoring_role_arn                   = var.database.monitoring_role_arn
  performance_insights_enabled          = var.database.performance_insights_enabled
  performance_insights_kms_key_id       = local.kms_performance_insight_key_id
  performance_insights_retention_period = var.database.performance_insights_retention_period
  preferred_backup_window               = var.database.backup_window
  preferred_maintenance_window          = var.database.maintenance_window
  promotion_tier                        = var.database.promotion_tier
  publicly_accessible                   = var.database.publicly_accessible
  tags                                  = merge(var.tags, var.database.tags)
}

output "aws_rds_cluster" {
  value = var.database.cluster ? { for k, v in aws_rds_cluster.this["0"] :
    k => v if !contains(["tags", "master_password", "enabled_cloudwatch_logs_exports", "iops", "availability_zones", "master_password_wo", "password_wo"], k)
  } : null
}

#####
# SSM
#####

locals {
  ssm_parameters_names = var.create_ssm_parameters ? concat(
    var.ssm_parameters_export_endpoint ? [var.ssm_parameters_endpoint_key_name] : [],
    var.ssm_parameters_export_port ? [var.ssm_parameters_port_key_name] : [],
    var.ssm_parameters_export_master_username ? [var.ssm_parameters_master_username_key_name] : [],
    var.ssm_parameters_export_master_password ? [var.ssm_parameters_master_password_key_name] : [],
    var.ssm_parameters_export_database_name ? [var.ssm_parameters_database_name_key_name] : [],
    var.ssm_parameters_export_character_set_name ? [var.ssm_parameters_character_set_name_key_name] : [],
    var.ssm_parameters_export_endpoint_reader ? [var.ssm_parameters_endpoint_reader_key_name] : [],
  ) : []
}

module "ssm" {
  source  = "gitlab.com/wild-beavers/module-aws-ssm-parameters/aws"
  version = "~> 6"

  prefix = var.ssm_parameters_prefix

  names = local.ssm_parameters_names
  types = concat(
    var.ssm_parameters_export_endpoint ? ["String"] : [],
    var.ssm_parameters_export_port ? ["String"] : [],
    var.ssm_parameters_export_master_username ? ["SecureString"] : [],
    var.ssm_parameters_export_master_password ? ["SecureString"] : [],
    var.ssm_parameters_export_database_name ? ["String"] : [],
    var.ssm_parameters_export_character_set_name ? ["String"] : [],
    var.ssm_parameters_export_endpoint_reader ? ["String"] : [],
  )
  values = concat(
    var.ssm_parameters_export_endpoint ? [try(aws_rds_cluster.this["0"].endpoint, aws_db_instance.this["0"].address, "N/A")] : [],
    var.ssm_parameters_export_port ? [try(aws_rds_cluster.this["0"].port, aws_db_instance.this["0"].port, "N/A")] : [],
    var.ssm_parameters_export_master_username ? [var.database.username != null ? var.database.username : "N/A"] : [],
    var.ssm_parameters_export_master_password ? [var.secret != null && !try(var.secret.use_provided_password, false) ? jsondecode(data.aws_secretsmanager_secret_version.this["0"].secret_string)[var.secret.password_key] : (var.database.password != null ? var.database.password : "N/A")] : [],
    var.ssm_parameters_export_database_name ? [var.database.db_name != null ? var.database.db_name : "N/A"] : [],
    var.ssm_parameters_export_character_set_name ? [var.database.character_set_name != null ? var.database.character_set_name : "N/A"] : [],
    var.ssm_parameters_export_endpoint_reader ? [(var.database.cluster ? aws_rds_cluster.this["0"].reader_endpoint : "N/A")] : [],
  )
  descriptions = concat(
    var.ssm_parameters_export_endpoint ? [var.ssm_parameters_endpoint_description] : [],
    var.ssm_parameters_export_port ? [var.ssm_parameters_port_description] : [],
    var.ssm_parameters_export_master_username ? [var.ssm_parameters_master_username_description] : [],
    var.ssm_parameters_export_master_password ? [var.ssm_parameters_master_password_description] : [],
    var.ssm_parameters_export_database_name ? [var.ssm_parameters_database_name_description] : [],
    var.ssm_parameters_export_character_set_name ? [var.ssm_parameters_character_set_name_description] : [],
    var.ssm_parameters_export_endpoint_reader ? [var.ssm_parameters_endpoint_reader_description] : [],
  )

  use_default_kms_key = false
  kms_key_arn         = local.kms_ssm_parameter_key_id
  kms_key_create      = false

  iam_policy_create                 = var.ssm_parameters_iam_policy_create
  iam_policy_path                   = var.ssm_parameters_iam_policy_path
  iam_policy_name_prefix_read_only  = format("%s%s", var.testing_prefix, var.ssm_parameters_iam_policy_name_prefix_read_only)
  iam_policy_name_prefix_read_write = format("%s%s", var.testing_prefix, var.ssm_parameters_iam_policy_name_prefix_read_write)

  kms_tags = merge(
    var.tags,
    local.tags,
    var.ssm_parameters_kms_key_tags,
  )

  tags = merge(
    var.tags,
    local.tags,
    var.ssm_parameters_tags,
  )
}
