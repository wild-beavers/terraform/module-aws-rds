####
# Variables
####

variable "secret" {
  description = <<-DOCUMENTATION
Store the RDS username and password in a Secrets Manager Secret.
Similar feature as `var.database.manage_master_user_password`.
One may prefer this over `var.database.manage_master_user_password` to fine-grained the SecretsManager Secret characteristics like the KMS key, the JSON format, rotation lambda, or the resource-based policy for better security and convenience.
Therefore, this variable conflicts with `var.database.manage_master_user_password`.
  - name                               (optional, string, "database_secret"): Name of the Secrets Manager Secret.
  - description                        (optional, string, dynamic):           Description of the Secrets Manager Secret.
  - use_provided_password              (optional, bool, true):                Store the secret provided in `var.database.password`. You might want to disable this to force the Secret Manager Secret to NOT contain the real database password, in the intention to immediate secret rotation. This is recommended to avoid storing secret in the state file, but require external automation or manual intervention.
  - username_key                       (optional, string, "username"):        Name of the JSON key containing the username of the RDS.
  - password_key                       (optional, string, "password"):        Name of the JSON key containing the password of the RDS.
  - iam_policy_entity_arns             (required, map(map(string)), {}):      Restrict access the given IAM entities (roles or users). Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them. `full` is mandatory to avoid the secret being unmanageable.
  - iam_policy_export_actions          (optional, bool, false):               Whether to output IAM policies actions for the Secrets Manager Secret. A lightweight way to generate identity-based policies.
  - iam_policy_identity_export_jsons   (optional, bool, false):               Whether to export identity-based IAM policies contents as JSON for the Secrets Manager Secret. Setting this to `true` do not create any policies.
  - iam_policy_restrict_by_account_ids (optional, list(string), []):          Restrict IAM Policy by the given account IDs. Resource-based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards.
  - iam_policy_restrict_by_regions     (optional, list(string), []):          Restrict resource-based IAM Policy by the given regions. Resource-Based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains region wildcards.
  - iam_policy_sid_prefix              (optional, string, ""):                Use a prefix for all `Sid` of all the policies created by the secret.
  - iam_policy_source_arns             (optional, map(map(string)), {}):      Restrict accesses to the given source ARNs. Wildcards are allowed. Keys are scope: `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities defined within.
  - iam_policy_source_policy_documents (optional, list(string), []):          List of JSON strings to be concatenated to the resource-based IAM Policy.
  - kms_key_id                         (optional, string):                    KMS key ID (or alias) to use to store the Secret Managers Secret. Conflicts with `var.kms_keys.secrets_manager`. If neither this argument not `var.kms_keys.secrets_manager` are set, the default KMS for Secrets Manager will be used.
  - kms_key_replica_id                 (optional, string):                    KMS key replica ID (or replica alias) to use to store the replica of the secret. Conflicts with `var.kms_keys.secrets_manager`. If neither this argument not `var.kms_keys.secrets_manager` are set, the default KMS for Secrets Manager will be used.
  - recovery_window_in_days            (optional, number, 7):                 The number of days that AWS Secrets Manager waits before it can delete the secret. This value can be `0` to force deletion without recovery or range from `7` to `30` days.
  - rotation_automatically_after_days  (optional, number):                    Specifies the number of days between automatic scheduled rotations of the secret. Needs `rotation_lambda_arn`.
  - rotation_duration                  (optional, number, 24):                The length of the rotation window in hours. For example, `3` for a three hour window. Needs `rotation_lambda_arn`.
  - rotation_enabled                   (optional, bool):                      Whether to enable secret rotation. Needs `rotation_lambda_arn`
  - rotation_lambda_arn                (optional, string):                    Specifies the ARN of the Lambda function that can rotate the secret. If `null`, the secret will not be rotated.
  - rotation_schedule_expression       (optional, string):                    A `cron()` or `rate()` expression that defines the schedule for rotating your secret. Either `automatically_after_days` or `schedule_expression` must be specified. Needs `rotation_lambda_arn`.
  - tags                               (optional, map(string), {}):           Tags specific to the Secrets Manager Secret. Will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name                               = optional(string, "")
    description                        = optional(string, "")
    use_provided_password              = optional(bool, true)
    username_key                       = optional(string, "username")
    password_key                       = optional(string, "password")
    iam_policy_entity_arns             = optional(map(map(string)), {})
    iam_policy_export_actions          = optional(bool, false)
    iam_policy_identity_export_jsons   = optional(bool, false)
    iam_policy_restrict_by_account_ids = optional(list(string), [])
    iam_policy_restrict_by_regions     = optional(list(string), [])
    iam_policy_sid_prefix              = optional(string, "")
    iam_policy_source_arns             = optional(map(map(string)), {})
    iam_policy_source_policy_documents = optional(list(string), [])
    kms_key_id                         = optional(string)
    kms_key_replica_id                 = optional(string)
    recovery_window_in_days            = optional(number, 7)
    rotation_automatically_after_days  = optional(number)
    rotation_duration                  = optional(number, 24)
    rotation_enabled                   = optional(bool, false)
    rotation_lambda_arn                = optional(string)
    rotation_schedule_expression       = optional(string)
    tags                               = optional(map(string), {})
  })
  default = null

  validation {
    condition     = !(var.secret != null && var.database.manage_master_user_password)
    error_message = "“var.secret” cannot be set as the same time as “var.database.manage_master_user_password”."
  }
  validation {
    condition     = var.secret == null || try(var.secret.kms_key_id, null) == null || lookup(var.kms_keys, "secrets_manager", null) == null
    error_message = "“var.secret.kms_key_id” cannot be given along with “var.kms_keys.secrets_manager”."
  }
  validation {
    condition     = var.secret == null || try(var.secret.kms_key_replica_id, null) == null || lookup(var.kms_keys, "secrets_manager", null) == null
    error_message = "“var.secret.kms_key_replica_id” cannot be given along with “var.kms_keys.secrets_manager”."
  }
}

####
# Resources
####

locals {
  secrets_kms_key_id         = try(var.secret.kms_key_id, null)
  secrets_kms_key_replica_id = var.replica_enabled ? try(var.secret.kms_key_replica_id, null) : null
}

# With Terraform 1.10, it may be possible to hide the secret from the statefile.
data "aws_secretsmanager_secret_version" "this" {
  for_each = var.secret != null && !try(var.secret.use_provided_password, false) ? { 0 = "enabled" } : {}

  secret_id = module.secret["0"].aws_secretsmanager_secrets["0"].arn

  depends_on = [
    time_sleep.wait_secret_manager_password["0"]
  ]
}

# As many resources, AWS API returns too early, thus making the "normal" dependency insufficient to get the secret value
# on first creation. Therefor, we wait for the amount of time below, to make sure the Secret Manager Secret is ready.
resource "time_sleep" "wait_secret_manager_password" {
  for_each = var.secret != null && !try(var.secret.use_provided_password, false) ? { 0 = "enabled" } : {}

  create_duration = "5s"

  depends_on = [
    module.secret["0"],
  ]
}

module "secret" {
  source  = "gitlab.com/wild-beavers/module-aws-secret-manager/aws"
  version = "~> 3"

  for_each = var.secret != null ? { 0 = "enabled" } : {}

  current_account_id = var.current_account_id
  current_region     = var.current_region
  replica_region     = var.replica_region

  secrets = {
    0 = {
      description = var.secret.description == "" ? "Credentials ${local.default_description}." : var.secret.description
      secrets = {
        (var.secret.username_key) = var.database.username
        (var.secret.password_key) = var.secret.use_provided_password ? var.database.password : "THIS_IS_NOT_THE_REAL_RDS_PASSWORD_CHANGE_IT"
      },
      name                    = "${var.testing_prefix}${var.secret.name}"
      recovery_window_in_days = var.secret.recovery_window_in_days

      rotation_enabled                  = var.secret.rotation_enabled
      rotation_lambda_arn               = var.secret.rotation_lambda_arn
      rotation_automatically_after_days = var.secret.rotation_automatically_after_days
      rotation_schedule_expression      = var.secret.rotation_schedule_expression
      rotation_duration                 = var.secret.rotation_duration
    }
  }

  replica_enabled = var.replica_enabled

  kms_key            = lookup(var.kms_keys, "secrets_manager", null)
  kms_key_id         = local.secrets_kms_key_id
  kms_key_replica_id = local.secrets_kms_key_replica_id

  iam_policy_entity_arns             = var.secret.iam_policy_entity_arns
  iam_policy_export_actions          = var.secret.iam_policy_export_actions
  iam_policy_identity_export_jsons   = var.secret.iam_policy_identity_export_jsons
  iam_policy_restrict_by_account_ids = var.secret.iam_policy_restrict_by_account_ids
  iam_policy_restrict_by_regions     = var.secret.iam_policy_restrict_by_regions
  iam_policy_sid_prefix              = var.secret.iam_policy_sid_prefix
  iam_policy_source_arns             = var.secret.iam_policy_source_arns
  iam_policy_source_policy_documents = var.secret.iam_policy_source_policy_documents

  secret_tags = var.secret.tags
  tags        = var.tags

  providers = {
    aws.replica = aws.replica
  }
}

####
# Outputs
####

output "aws_secretsmanager_secret" {
  value = var.secret != null ? module.secret["0"].aws_secretsmanager_secrets["0"] : null
}

output "secret_aws_iam_policies" {
  value = var.secret != null ? module.secret["0"].aws_iam_policies : null
}

output "secret_aws_kms_key" {
  value = lookup(var.kms_keys, "secrets_manager", null) != null ? module.secret["0"].aws_kms_keys["0"] : null
}

output "secret_aws_kms_alias" {
  value = lookup(var.kms_keys, "secrets_manager", null) != null ? module.secret["0"].aws_kms_aliases["0"] : null
}

output "secret_replica_aws_kms_key" {
  value = lookup(var.kms_keys, "secrets_manager", null) != null && var.replica_enabled ? module.secret["0"].replica_aws_kms_keys["0"] : null
}

output "secret_replica_aws_kms_alias" {
  value = lookup(var.kms_keys, "secrets_manager", null) != null && var.replica_enabled ? module.secret["0"].replica_aws_kms_aliases["0"] : null
}
