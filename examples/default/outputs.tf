output "default" {
  value = module.default
}

output "alternative" {
  value = module.alternative
}

output "cluster" {
  value = module.cluster
}
