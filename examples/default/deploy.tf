data "aws_caller_identity" "current" {}

data "aws_region" "current" {}
data "aws_region" "replica" {
  provider = aws.replica
}

data "aws_partition" "current" {}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

data "aws_rds_engine_version" "postgres" {
  engine = "postgres"
  latest = true
}

data "aws_rds_engine_version" "mysql" {
  engine = "mysql"
  latest = true
}

locals {
  testing_prefix = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn     = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_security_group" "example" {
  name        = "${local.testing_prefix}tftestexample"
  description = "test"
  vpc_id      = data.aws_vpc.default.id
}

module "kms_example" {
  source  = "gitlab.com/wild-beavers/module-aws-kms/aws"
  version = "~> 2"

  prefix = local.testing_prefix
  kms_keys = {
    minimal = {
      alias                   = "example-external"
      deletion_window_in_days = 7
    }
  }

  replica_enabled = false

  iam_policy_entity_arns = {
    full = { 0 = local.caller_arn }
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Default
#####

module "default" {
  source = "../../"

  testing_prefix = local.testing_prefix
  vpc_id         = data.aws_vpc.default.id

  database = {
    cluster = false

    allocated_storage = 20
    engine            = data.aws_rds_engine_version.postgres.engine
    instance_class    = "db.t3.large"

    allow_major_version_upgrade           = true
    apply_immediately                     = true
    auto_minor_version_upgrade            = true
    backup_retention_period               = 0
    database_name                         = "tftest"
    deletion_protection                   = false
    engine_version                        = data.aws_rds_engine_version.postgres.version_actual
    identifier                            = "tftest"
    maintenance_window                    = "wed:04:00-wed:04:30"
    manage_master_user_password           = true
    max_allocated_storage                 = 25
    multi_az                              = true
    performance_insights_enabled          = true
    performance_insights_retention_period = 7
    port                                  = 3333
    skip_final_snapshot                   = true
    storage_type                          = "gp3"
    storage_encrypted                     = true
    username                              = "tftest"
  }

  db_subnet_group = {
    name = "tftest"
    tags = {
      dbsubnetgrouptag = "tftest"
    }
    subnet_ids = data.aws_subnets.default.ids
  }

  parameter_group = {
    name   = "tftest"
    family = "postgres17"
    tags = {
      is_test = "yes"
    }
  }
  parameter_group_parameters = [
    {
      name  = "auto_explain.log_analyze"
      value = "1"
    },
    {
      name         = "auto_explain.log_verbose"
      value        = "1"
      apply_method = "pending-reboot"
    }
  ]

  replica_enabled = false
  kms_iam_policy_entity_arns = {
    full = {
      admin = local.caller_arn
    }
  }
  kms_keys = {
    db = {
      alias                   = "tftest"
      deletion_window_in_days = 7
    }
    performance_insight = {
      alias                   = "tftestperf"
      deletion_window_in_days = 7
    }
    ssm_parameter = {
      alias                   = "tftestssm"
      deletion_window_in_days = 7
    }
  }

  security_group_ids = [aws_security_group.example.id]
  security_group = {
    name               = "tftest"
    allowed_ipv4_cidrs = ["172.17.23.23/32"]
    tags = {
      is_test = "security_group"
    }
  }

  create_ssm_parameters                            = true
  ssm_parameters_prefix                            = "${random_string.this.result}2"
  ssm_parameters_endpoint_key_name                 = "endpointURL"
  ssm_parameters_iam_policy_create                 = true
  ssm_parameters_iam_policy_name_prefix_read_only  = "tftestRO"
  ssm_parameters_iam_policy_name_prefix_read_write = "tftestRW"

  ssm_parameters_tags = {
    ssmTags = "foo"
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Alternative
#####

module "alternative" {
  source = "../../"

  testing_prefix = local.testing_prefix
  vpc_id         = data.aws_vpc.default.id

  database = {
    cluster = false

    allocated_storage = 20
    engine            = data.aws_rds_engine_version.mysql.engine
    instance_class    = "db.t3.large"

    current_account_id = data.aws_caller_identity.current.account_id
    current_partition  = data.aws_partition.current.partition
    current_region     = data.aws_region.current.name
    replica_region     = data.aws_region.replica.name

    availability_zones                    = sort(data.aws_availability_zones.available.names)
    allow_major_version_upgrade           = true
    max_allocated_storage                 = 20
    multi_az                              = false
    storage_type                          = "gp3"
    engine_version                        = "5.7"
    deletion_protection                   = false
    apply_immediately                     = true
    auto_minor_version_upgrade            = true
    identifier                            = "tftest2"
    database_name                         = "tftest2"
    performance_insights_enabled          = true
    performance_insights_retention_period = 7
    password                              = "tftest2notrandom"
    username                              = "tftest2"
    skip_final_snapshot                   = true
    storage_encrypted                     = true
    maintenance_window                    = "sat:01:00-sat:02:00"
  }

  kms_key_id                     = module.kms_example.aws_kms_keys["minimal"].arn
  kms_performance_insight_key_id = module.kms_example.aws_kms_keys["minimal"].arn
  kms_ssm_parameter_key_id       = module.kms_example.aws_kms_keys["minimal"].arn

  db_subnet_group = {
    name = "tftest2"
    tags = {
      dbsubnetgrouptag = "tftest2"
    }
    subnet_ids = data.aws_subnets.default.ids
  }

  security_group = {
    name                           = "tftest2"
    allowed_ipv4_cidrs             = ["127.0.0.1/32", "10.0.0.0/8"]
    allowed_source_security_groups = { 0 = aws_security_group.example }
    allowed_cidrs_description      = "From all our subnetwork at contoso company"
    tags = {
      dbsgtags = "tftest2"
    }
  }

  option_group = {
    name                 = "tftest2"
    engine_name          = "mysql"
    major_engine_version = regex("^\\d+\\.\\d", "5.7")
    tags = {
      is_test = "yes"
    }
  }
  option_group_options = [
    {
      option_name                    = "MEMCACHED",
      port                           = 11211,
      vpc_security_group_memberships = [aws_security_group.example.id],
      option_settings = [
        {
          name  = "INNODB_API_DISABLE_ROWLOCK",
          value = "1",
        },
        {
          name  = "DAEMON_MEMCACHED_W_BATCH_SIZE",
          value = "42",
        }
      ],
    },
    {
      option_name = "MARIADB_AUDIT_PLUGIN",
      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS",
          value = "QUERY",
        }
      ]
    }
  ]

  #####
  # SSM parameters
  #####

  create_ssm_parameters = true

  ssm_parameters_prefix = random_string.this.result

  ssm_parameters_export_master_password    = false
  ssm_parameters_export_database_name      = false
  ssm_parameters_export_character_set_name = false
  ssm_parameters_export_endpoint_reader    = false

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Cluster
#####

module "cluster" {
  source = "../../"

  testing_prefix = local.testing_prefix
  vpc_id         = data.aws_vpc.default.id

  database = {
    cluster = true

    allocated_storage = 20
    engine            = data.aws_rds_engine_version.postgres.engine
    instance_class    = "db.m6gd.large"

    size = 0

    availability_zones           = sort(slice(data.aws_availability_zones.available.names, 0, 2))
    allow_major_version_upgrade  = true
    storage_type                 = "gp3"
    cluster_identifier           = "tftest3"
    engine_version               = data.aws_rds_engine_version.postgres.version_actual
    deletion_protection          = false
    apply_immediately            = true
    auto_minor_version_upgrade   = true
    identifier                   = "tftest3"
    database_name                = "tftest3"
    performance_insights_enabled = false
    password                     = "tftest3notrandom"
    username                     = "tftest3"
    skip_final_snapshot          = true
    storage_encrypted            = true
    maintenance_window           = "sat:01:00-sat:02:00"
    promotion_tiers              = 0
  }

  db_subnet_group = {
    name = "tftest3"
    tags = {
      dbsubnetgrouptag = "tftest3"
    }
    subnet_ids = data.aws_subnets.default.ids
  }

  security_group = {
    name                           = "tftest3"
    allowed_source_security_groups = { 0 = aws_security_group.example }
    tags = {
      dbsgtags = "tftest3"
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}
