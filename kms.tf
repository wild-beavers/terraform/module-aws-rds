####
# Variables
####

variable "kms_key_id" {
  description = "ARN or ID (or alias) of the AWS KMS customer master key (CMK) to be used to encrypt all the database data. If you don’t specify this value or `var.kms_keys.db`, defaults to using the AWS account’s default RDS KMS key."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = !(var.kms_key_id != "" && lookup(var.kms_keys, "db", null) != null)
    error_message = "“var.kms_key_id” cannot be given along with “var.kms_keys.db”"
  }
}

variable "kms_performance_insight_key_id" {
  description = "ARN or ID (or alias) of the AWS KMS customer master key (CMK) to be used to encrypt all the performance insight data. If you don’t specify this value or `var.kms_keys.performance_insight`, defaults to using the AWS account’s default RDS KMS key."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = !(var.kms_performance_insight_key_id != "" && lookup(var.kms_keys, "performance_insight", null) != null)
    error_message = "“var.kms_performance_insight_key_id” cannot be given along with “var.kms_keys.performance_insight”"
  }
}

variable "kms_ssm_parameter_key_id" {
  description = "ARN or ID (or alias) of the AWS KMS customer master key (CMK) to be used to encrypt all the SSM Parameters. If you don’t specify this value or `var.kms_keys.ssm_parameter`, defaults to using the AWS account’s default SSM KMS key."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = !(var.kms_ssm_parameter_key_id != "" && lookup(var.kms_keys, "ssm_parameter", null) != null)
    error_message = "“var.kms_ssm_parameter_key_id” cannot be given along with “var.kms_keys.ssm_parameter”"
  }
}

variable "kms_iam_policy_entity_arns" {
  description = <<-DOCUMENTATION
Restrict KMS keys access the given IAM entities (roles or users).
Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them.
Wildcards are allowed.
DOCUMENTATION
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition     = length(var.kms_keys) == 0 || length(lookup(var.kms_iam_policy_entity_arns, "full", {})) > 0
    error_message = "Since “var.kms_keys” were given, at least one “var.kms_iam_policy_entity_arns.full” is required."
  }
}

variable "kms_iam_policy_source_arns" {
  description = <<-DOCUMENTATION
Restrict access to the KMS keys to the given ARN sources.
Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them.
Wildcards are allowed.
DOCUMENTATION
  type        = map(map(string))
  default     = {}
  nullable    = false
}

variable "kms_iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of KMS keys policies - internal and external - created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "kms_iam_policy_restrict_by_account_ids" {
  description = "Restrict KMS keys created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "kms_iam_policy_restrict_by_regions" {
  description = "Restrict KMS keys created by this module by the given regions. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains region wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "kms_tags" {
  description = "Tags to be shared with all KMS keys created by this module."
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "kms_keys" {
  description = <<-DOCUMENTATION
Options of the KMS keys to create, for various resources in this module.
Would you wish to reuse the same KMS key for multiple resources, use the `var.kms_***_kms_id` variables.
Allowed keys are "db", "ssm_parameter", "performance_insight" or "secrets_manager":

  - alias                   (required, string):                      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  - principal_actions       (optional, map(list(string))):           List of read-only, read-write, read-write-delete, audit and full allowed IAM actions for the principals defined by "var.iam_policy_entity_arns[key]" variable, or nobody, if variable value is undefined. The four expected keys are "ro", "rw", "rwd", "audit" and "full". If this is not provided, actions default to corresponding `var.iam_policy_external_actions_...`.
  - policy_json             (optional, string):                      Additional policy to attach to the KMS key, merged with a baseline internal policy.
  - description             (optional, string, ""):                  Description of the key.
  - rotation_enabled        (optional, bool, true):                  Whether to automatically rotate the KMS key linked to the secrets.
  - deletion_window_in_days (optional, number, 7):                   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  - tags                    (optional, map(string)):                 Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = map(object({
    alias                   = string
    principal_actions       = optional(map(list(string)))
    policy_json             = optional(string, null)
    description             = optional(string, "")
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  }))
  default  = {}
  nullable = false

  validation {
    condition = alltrue([for key, kms_key in var.kms_keys : (
      contains(["db", "ssm_parameter", "performance_insight", "secrets_manager"], key)
    )])
    error_message = "One or more of “var.kms_keys” keys are invalid. Allowed keys are: ${join(", ", ["db", "ssm_parameter", "performance_insight", "secrets_manager"])}. Given: ${join(", ", keys(var.kms_keys))}"
  }
}

####
# Resources
####

locals {
  # "secrets_manager" is voluntarily ignored, as it would be managed by the secrets-manager module
  kms_keys = merge({ for type, kms_key_data in var.kms_keys :
    type => merge(
      kms_key_data,
      {
        service_principal = "rds"
        service_actions = [
          "kms:CreateGrant",
          "kms:DescribeKey",
          "kms:RevokeGrant",
        ]
      },
    ) if contains(["db", "performance_insight"], type)
    },
    { for type, kms_key_data in var.kms_keys :
      type => merge(
        kms_key_data,
        {
          service_principal = "ssm"
          service_actions = [
            "kms:Encrypt",
            "kms:Decrypt",
            "kms:GenerateDataKey",
            "kms:DescribeKey",
          ]
        },
      ) if contains(["ssm_parameter"], type)
    }
  )

  # RDS causes idempotency issues when using alias instead of ID
  kms_key_id                     = var.kms_key_id != "" ? var.kms_key_id : (lookup(var.kms_keys, "db", null) != null ? module.kms["0"].aws_kms_keys.db.arn : null)
  kms_performance_insight_key_id = var.kms_performance_insight_key_id != "" ? var.kms_performance_insight_key_id : (lookup(var.kms_keys, "performance_insight", null) != null ? module.kms["0"].aws_kms_keys.performance_insight.arn : null)
  kms_ssm_parameter_key_id       = var.kms_ssm_parameter_key_id != "" ? var.kms_ssm_parameter_key_id : (lookup(var.kms_keys, "ssm_parameter", null) != null ? module.kms["0"].aws_kms_aliases.ssm_parameter.arn : null)
}

module "kms" {
  source  = "gitlab.com/wild-beavers/module-aws-kms/aws"
  version = "~> 2"

  for_each = length(local.kms_keys) > 0 ? { 0 = "enabled" } : {}

  prefix = var.testing_prefix

  kms_keys = local.kms_keys

  iam_policy_entity_arns             = var.kms_iam_policy_entity_arns
  iam_policy_restrict_by_account_ids = var.kms_iam_policy_restrict_by_account_ids
  iam_policy_restrict_by_regions     = var.kms_iam_policy_restrict_by_regions
  iam_policy_sid_prefix              = var.kms_iam_policy_sid_prefix
  iam_policy_source_arns             = var.kms_iam_policy_source_arns

  tags = var.kms_tags

  replica_enabled = var.replica_enabled

  providers = {
    aws.replica = aws.replica
  }
}

####
# Outputs
####

output "aws_kms_keys" {
  value = length(local.kms_keys) > 0 ? module.kms["0"].aws_kms_keys : null
}

output "aws_kms_aliases" {
  value = length(local.kms_keys) > 0 ? module.kms["0"].aws_kms_aliases : null
}

output "replica_aws_kms_keys" {
  value = length(local.kms_keys) > 0 && var.replica_enabled ? module.kms["0"].replica_aws_kms_keys : null
}

output "replica_aws_kms_aliases" {
  value = length(local.kms_keys) > 0 && var.replica_enabled ? module.kms["0"].replica_aws_kms_aliases : null
}
