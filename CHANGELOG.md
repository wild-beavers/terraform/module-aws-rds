## 6.0.2

- fix: filter out sensitive attributes from outputs.

## 6.0.1

- maintenance: migrate `wild-beavers` modules to Terraform registry
- chore: bump pre-commit hooks

## 6.0.0

- tech: (BREAKING) now requires terraform 1.9+
- refactor: (BREAKING) destructive changes in the following resources to better manage pluralization:
   - `aws_db_subnet_group`
   - `aws_db_parameter_group`
   - `aws_db_option_group`
   - `aws_rds_cluster_parameter_group`
   - `aws_security_group`
- feat: (BREAKING) removes old compatibility Terraform variables:
   - `var.enabled`
   - `var.use_default_kms_key`
   - `var.kms_key_alias_name`
   - `var.kms_key_create`
   - `var.kms_key_create_alias`
   - `var.kms_key_name`
   - `var.kms_key_policy_json`
   - `var.kms_key_tags`
- feat: (BREAKING) removes outputs:
   - `kms_key_id`
   - `kms_key_arn`
   - `kms_key_alias_arn`
   - `kms_key_alias_target_key_arn`
- feat: (BREAKING) removes the ability to set suffixes in names: `var.use_num_suffix` and `var.num_suffix_digits`
- feat: (BREAKING) introduces `var.kms_keys` along with new `var.kms_***` to manage KMS keys in a more complete way
- feat: (BREAKING) introduces `var.parameter_group` replacing `var.parameter_group_***` to manage DB Parameter Group
- feat: (BREAKING) introduces `var.option_group` replacing `var.option_group_***` to manage DB Option Group
- feat: (BREAKING) introduces `var.database` replacing most base variables to manage DB and DB clusters
- feat: removes deprecated `Terraform` tag

## 5.0.0

- fix: (BREAKING) `aws_db_instance` attribute change from `name` to `db_name` sets minimum aws provider version to 5.
- maintenance: upgrade ssm parameter module to version 6 to avoid pinning aws provider version to an upper limit.
- doc: use new changelog format.
- tech: update precommit and fix failing checks.
- fix: no longer required to set `var.rds_instance_availability_zones` when deploying aurora db.
- fix: remove unused variable `rds_cluster_enable_s3_import`
- tech: adds gitlab ci config file.

## 4.0.0

- fix (BREAKING): remove `security_group_ingress` and `security_group_egress` output (idempotency issue)

## 3.0.3

- feat: change ssm module from FX to wildbeaver
- test: remove Jenkinsfile
- chore: bump pre-commit hook version

## 3.0.2

- feat: update pre-commit
- fix: main.tf change ssm version from 3.0.1 to 3.0.2

## 3.0.1

- chore: bump pre-commit hooks
- fix: in versions.tf change from `~> 4` to `>= 4, < 4.0`

## 3.0.0

- feat: (BREAKING) Create SG rule for client and rename variable

## 2.1.0

- maintenance: bump SSM parameters module to 3.0.1
- fix: typo in versions.tf to be usable with terraform 0.13

## 2.0.0

- fix (BREAKING): rename `ssm_parameters_master_password_key_name` and `ssm_parameters_master_password_description`

## 1.0.0

- feat: init
