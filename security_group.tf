#####
# Variables
#####

variable "security_group_ids" {
  description = "IDs of the security groups to attach to the RDS instance (or cluster). Additionally, one may create another Security Group through `var.security_group_name`."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition     = alltrue([for v in var.security_group_ids : can(regex("^sg\\-[a-z0-9]{17}$", v))])
    error_message = "One or more “var.security_group_ids” are invalid."
  }
}

variable "security_group" {
  description = <<-DOCUMENTATION
Security Group to create for the database.
If this variable is not set, `var.security_group_ids` is mandatory.
  - name                           (required, string): Name of the security group to create.
  - allowed_ipv4_cidrs             (optional, list(string)): CIDRs (ipv4) allowed to communicate to the database.
  - allowed_ipv6_cidrs             (optional, list(string)): CIDRs (ipv6) allowed to communicate to the database.
  - allowed_cidrs_description      (optional, string): Description of what the CIDRs are for the Security Group rule.
  - allowed_source_security_groups (optional, map(object)):
    - id   (required, string): Id of a source Security Group
    - name (required, string): Corresponding name of the source Security Group
  - tags                           (optional, map(string): Tags for the Security Group created in this module. Will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name                      = string
    allowed_ipv4_cidrs        = optional(list(string), [])
    allowed_ipv6_cidrs        = optional(list(string), [])
    allowed_cidrs_description = optional(string, "")
    allowed_source_security_groups = optional(map(object({
      id   = optional(string)
      name = optional(string)
    })), {})
    tags = optional(map(string), {})
  })
  default = null

  validation {
    condition     = length(var.security_group_ids) > 0 || var.security_group != null
    error_message = "Either “var.security_group_ids” or “var.security_group” must be set."
  }
  validation {
    condition     = var.security_group == null || can(regex("^[a-zA-Z0-9\\._\\-:\\/\\(\\)\\#,\\@\\[\\]\\+\\=\\&;\\{\\}\\!\\$\\* ]{1,255}$", try(var.security_group.name)))
    error_message = "“var.security_group.name” is invalid."
  }
  validation {
    condition     = alltrue([for cidr in try(var.security_group.allowed_ipv4_cidrs, []) : can(cidrhost(cidr, "0"))])
    error_message = "One or more “var.security_group.allowed_ipv4_cidrs” are invalid."
  }
  validation {
    condition     = alltrue([for cidr in try(var.security_group.allowed_ipv6_cidrs, []) : can(cidrhost(cidr, "0"))])
    error_message = "One or more “var.security_group.allowed_ipv6_cidrs” are invalid."
  }
  validation {
    condition     = alltrue([for v in try(var.security_group.allowed_source_security_groups, []) : can(regex("^sg\\-[a-z0-9]{17}$", v.id))])
    error_message = "One or more “var.security_group.allowed_source_security_groups” are invalid."
  }
}

#####
# Resources
#####

resource "aws_security_group" "this" {
  for_each = var.security_group != null ? { 0 = "enabled" } : {}

  name        = "${var.testing_prefix}${var.security_group.name}"
  description = local.default_description
  vpc_id      = var.vpc_id
  tags = merge(
    local.tags,
    var.security_group.tags,
    {
      Name        = "${var.testing_prefix}${var.security_group.name}"
      Description = local.default_description
    },
  )
}

resource "aws_security_group_rule" "this_in_cidr" {
  for_each = var.security_group != null && (length(var.security_group.allowed_ipv4_cidrs) > 0 || length(var.security_group.allowed_ipv6_cidrs) > 0) ? { 0 = "enabled" } : {}

  type             = "ingress"
  from_port        = var.database.cluster ? aws_rds_cluster.this["0"].port : aws_db_instance.this["0"].port
  to_port          = var.database.cluster ? aws_rds_cluster.this["0"].port : aws_db_instance.this["0"].port
  protocol         = "tcp"
  cidr_blocks      = var.security_group.allowed_ipv4_cidrs
  ipv6_cidr_blocks = var.security_group.allowed_ipv6_cidrs

  security_group_id = aws_security_group.this["0"].id
}

resource "aws_security_group_rule" "this_in_sg" {
  for_each = var.security_group != null ? var.security_group.allowed_source_security_groups : {}

  description              = "From ${each.value.name}."
  type                     = "ingress"
  from_port                = var.database.cluster ? aws_rds_cluster.this["0"].port : aws_db_instance.this["0"].port
  to_port                  = var.database.cluster ? aws_rds_cluster.this["0"].port : aws_db_instance.this["0"].port
  protocol                 = "tcp"
  source_security_group_id = each.value.id

  security_group_id = aws_security_group.this["0"].id
}

#####
# Outputs
#####

output "aws_security_group" {
  value = var.security_group != null ? { for k, v in aws_security_group.this["0"] :
    k => v if !contains(["tags", "egress", "ingress", "name_prefix", "owner_id", "timeouts"], k)
  } : null
}
