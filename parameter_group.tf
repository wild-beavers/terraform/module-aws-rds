#####
# Variables
#####

variable "parameter_group" {
  description = <<-DOCUMENTATION
Parameter Group to create.
  - name         (required, string):          Name of the DB Parameter Group.
  - family       (required, string):          The family of the DB parameter group.
  - description  (optional, string, dynamic): Description of the DB parameter group.
  - skip_destroy (optional, bool, false):     Set to `true` if you do not wish the parameter group to be deleted at destroy time, and instead just remove the parameter group from the Terraform state.
  - tags         (optional, map(string), {}): Tags to assign to the resource, will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name         = string
    family       = string
    description  = optional(string, null)
    skip_destroy = optional(bool, false)
    tags         = optional(map(string), {})
  })
  default = null

  validation {
    # This regex is imperfect and will let invalid edge-case strings pass (strings over 255 when using dashes)
    condition     = var.parameter_group == null || can(regex("^(?:[a-zA-Z0-9]\\-?){0,254}[a-zA-Z0-9]$", var.parameter_group.name))
    error_message = "“var.parameter_group.name” is invalid."
  }
}

variable "parameter_group_parameters" {
  description = <<-DOCUMENTATION
Parameter Groups Parameters to add:
  - name         (required, string):       Name of the DB parameter.
  - value        (required, string):       Value of the DB parameter.
  - apply_method (optional, string, null): "immediate" (default), or "pending-reboot". Some engines can’t apply some parameters without a reboot, and you will need to specify "pending-reboot" here.
DOCUMENTATION

  type = list(object({
    name         = string
    value        = string
    apply_method = optional(string, null)
  }))
  default  = []
  nullable = false

  validation {
    condition = alltrue([for v in var.parameter_group_parameters : (
      contains(["immediate", "pending-reboot"], coalesce(v.apply_method, "immediate")) &&
      can(regex("^[a-zA-Z0-9_\\.\\-]+$", v.name))
    )])
    error_message = "One or more “var.parameter_group_parameters” are invalid."
  }
  validation {
    condition     = length(var.parameter_group_parameters) == 0 || var.parameter_group != null
    error_message = "“var.parameter_group” needs to be set when “var.parameter_group_parameters” are given."
  }
}

#####
# Resources
#####

locals {
  db_parameter_group_needed         = !var.database.cluster && var.parameter_group != null
  db_parameter_group_cluster_needed = var.database.cluster && var.parameter_group != null
}

resource "aws_db_parameter_group" "this" {
  for_each = local.db_parameter_group_needed ? { 0 = "enabled" } : {}

  name         = "${var.testing_prefix}${var.parameter_group.name}"
  family       = var.parameter_group.family
  description  = var.parameter_group.description != null ? var.parameter_group.description : local.default_description
  skip_destroy = var.parameter_group.skip_destroy

  dynamic "parameter" {
    for_each = var.parameter_group_parameters

    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = parameter.value.apply_method
    }
  }

  tags = merge(
    var.tags,
    var.parameter_group.tags,
    {
      Name        = "${var.testing_prefix}${var.parameter_group.name}"
      Description = var.parameter_group.description != null ? var.parameter_group.description : local.default_description
    },
    local.tags,
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_rds_cluster_parameter_group" "this" {
  for_each = local.db_parameter_group_cluster_needed ? { 0 = "enabled" } : {}

  name        = "${var.testing_prefix}${var.parameter_group.name}"
  family      = var.parameter_group.family
  description = var.parameter_group.description != null ? var.parameter_group.description : local.default_description

  dynamic "parameter" {
    for_each = var.parameter_group_parameters

    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = parameter.value.apply_method
    }
  }

  tags = merge(
    local.tags,
    var.parameter_group.tags,
    {
      Name        = "${var.testing_prefix}${var.parameter_group.name}"
      Description = var.parameter_group.description != null ? var.parameter_group.description : local.default_description
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}

#####
# Outputs
#####

output "aws_db_parameter_group" {
  value = local.db_parameter_group_needed ? { for k, v in aws_db_parameter_group.this["0"] :
    k => v if !contains(["tags", "name_prefix", "parameter"], k)
  } : null
}

output "aws_rds_cluster_parameter_group" {
  value = local.db_parameter_group_cluster_needed ? { for k, v in aws_rds_cluster_parameter_group.this["0"] :
    k => v if !contains(["tags", "name_prefix", "parameter"], k)
  } : null
}
