terraform {
  required_version = ">= 1.9"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 5"
      configuration_aliases = [aws.replica]
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0"
    }
  }
}
