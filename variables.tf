#####
# Common
#####
variable "current_account_id" {
  description = "The account where this module is run."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.current_account_id == "" || can(regex("^([0-9]{12})?$", var.current_account_id))
    error_message = "“var.current_account_id” does not match '^[0-9]{12}$'."
  }
}

variable "current_region" {
  description = "The region where this module is run."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.current_region == "" || can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.current_region))
    error_message = "“var.current_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "replica_region" {
  description = "The region where this module must replicate resources."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.replica_region == "" || var.replica_region != var.current_region
    error_message = "“var.replica_region” cannot be the same as “var.current_region”."
  }
  validation {
    condition     = can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.replica_region))
    error_message = "“var.replica_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "vpc_id" {
  description = "VPC ID where to install the resources of this module."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = !(var.security_group != null && var.vpc_id == "")
    error_message = "“var.vpc_id” is required when “var.security_group” is set."
  }
  validation {
    condition     = var.vpc_id == "" || can(regex("^vpc-([a-z0-9]{8}|[a-z0-9]{17})$", var.vpc_id))
    error_message = "“var.vpc_id” does not match “^vpc-([a-z0-9]{8}|[a-z0-9]{17})$”."
  }
}

variable "testing_prefix" {
  description = "Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.testing_prefix == "" || can(regex("^[a-z][a-zA-Z0-9]{3}$", var.testing_prefix))
    error_message = "“var.testing_prefix” does not match “^[a-z][a-zA-Z0-9]{3}$”."
  }
}

variable "tags" {
  description = "Tags to be used in every resources created by this module."
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "replica_enabled" {
  description = "Whether to replicate the resources of this module into the `aws.replica` region."
  type        = bool
  default     = true
  nullable    = false
}

#####
# SSM parameters
#####

variable "create_ssm_parameters" {
  description = "Create SSM parameters related to database information"
  type        = bool
  default     = false
}

variable "ssm_parameters_prefix" {
  description = "Prefix to be add on all SSM parameter keys. Cannot started by «/»."
  type        = string
  default     = ""
}

variable "ssm_parameters_export_endpoint" {
  description = "Export the endpoint name in a SSM parameter."
  type        = bool
  default     = true
}

variable "ssm_parameters_endpoint_key_name" {
  description = "Name of the endpoint SSM parameter key."
  type        = string
  default     = "endpoint"
}

variable "ssm_parameters_endpoint_description" {
  description = "Description of the endpoint SSM parameter."
  type        = string
  default     = "DNS address of the database"
}

variable "ssm_parameters_export_port" {
  description = "Export the database port in a SSM parameter."
  type        = bool
  default     = true
}

variable "ssm_parameters_port_key_name" {
  description = "Name of the database port SSM parameter key."
  type        = string
  default     = "databasePort"
}

variable "ssm_parameters_port_description" {
  description = "Description of the database port SSM parameter."
  type        = string
  default     = "Port of the database"
}

variable "ssm_parameters_export_master_username" {
  description = "Export the master username in a secure SSM parameter."
  type        = bool
  default     = true
}

variable "ssm_parameters_master_username_key_name" {
  description = "Name of the master username SSM parameter key."
  type        = string
  default     = "masterUsername"
}

variable "ssm_parameters_master_username_description" {
  description = "Description of the master username SSM parameter."
  type        = string
  default     = "Master username of the database"
}

variable "ssm_parameters_export_master_password" {
  description = "Export the master password in a secure SSM parameter."
  type        = bool
  default     = true
}

variable "ssm_parameters_master_password_key_name" {
  description = "Name of the master passsword SSM parameter key."
  type        = string
  default     = "masterPassword"
}

variable "ssm_parameters_master_password_description" {
  description = "Description of the master passsword SSM parameter."
  type        = string
  default     = "Master password of the database"
}

variable "ssm_parameters_export_database_name" {
  description = "Export the database name in a SSM parameter. If no database name are provisioned, SSM parameter value will be «N/A»"
  type        = bool
  default     = true
}

variable "ssm_parameters_database_name_key_name" {
  description = "Name of the database name SSM parameter key."
  type        = string
  default     = "databaseName"
}

variable "ssm_parameters_database_name_description" {
  description = "Description of the database name SSM parameter."
  type        = string
  default     = "Database name created by AWS"
}

variable "ssm_parameters_export_character_set_name" {
  description = "Export the character set namein a SSM parameter. If no character set name are provisioned, SSM parameter value will be «N/A»"
  type        = bool
  default     = true
}

variable "ssm_parameters_character_set_name_key_name" {
  description = "Name of the character set name SSM parameter key."
  type        = string
  default     = "characterSetName"
}

variable "ssm_parameters_character_set_name_description" {
  description = "Description of the character set name SSM parameter."
  type        = string
  default     = "Character set name of the database"
}

variable "ssm_parameters_export_endpoint_reader" {
  description = "Export the endpoint reader name in a SSM parameter. If provisioned engine isn’t aurora, SSM parameter value will be «N/A»"
  type        = bool
  default     = true
}

variable "ssm_parameters_endpoint_reader_key_name" {
  description = "Name of the endpoint reader SSM parameter key."
  type        = string
  default     = "endpointReader"
}

variable "ssm_parameters_endpoint_reader_description" {
  description = "Description of the endpoint reader SSM parameter."
  type        = string
  default     = "DNS address of the read only RDS cluser"
}

variable "ssm_parameters_iam_policy_create" {
  description = "Create iam policy for SSM parameters and KMS key access."
  type        = bool
  default     = false
}

variable "ssm_parameters_iam_policy_path" {
  description = "Path of the SSM parameters IAM policies."
  type        = string
  default     = null
}

variable "ssm_parameters_iam_policy_name_prefix_read_only" {
  description = "Name of the SSM parameters IAM read only policy."
  type        = string
  default     = ""
}

variable "ssm_parameters_iam_policy_name_prefix_read_write" {
  description = "Name of the SSM parameters IAM read write policy."
  type        = string
  default     = ""
}

variable "ssm_parameters_kms_key_tags" {
  description = "Tags to be merge with all SSM parameters KMS key resources."
  type        = map(string)
  default     = {}
}

variable "ssm_parameters_tags" {
  description = "Tags to be merge with all SSM parameters resources."
  type        = map(string)
  default     = {}
}
