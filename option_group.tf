#####
# Variables
#####

variable "option_group" {
  description = <<-DOCUMENTATION
Option Group to create:
  - name                 (required, string):          Name of the Option Group.
  - engine_name          (required, string):          Name of the engine that this option group should be associated with.
  - major_engine_version (required, string):          Major version of the engine that this option group should be associated with.
  - description          (optional, string, dynamic): Description of the DB option group.
  - skip_destroy         (optional, bool, false):     Set to `true` if you do not wish the option group to be deleted at destroy time, and instead just remove the option group from the Terraform state.
  - tags                 (optional, map(string), {}): Tags to assign to the resource, will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name                 = string
    engine_name          = string
    major_engine_version = string
    description          = optional(string, null)
    skip_destroy         = optional(bool, false)
    tags                 = optional(map(string), {})
  })
  default = null

  validation {
    condition     = var.option_group == null || !var.database.cluster
    error_message = "“var.option_group” is not compatible with a cluster-type database."
  }
  validation {
    # This regex is imperfect and will let invalid edge-case strings pass (strings over 255 when using dashes)
    condition     = var.option_group == null || can(regex("^(?:[a-zA-Z0-9]\\-?){0,254}[a-zA-Z0-9]$", var.option_group.name))
    error_message = "“var.option_group.name” is invalid."
  }
}

variable "option_group_options" {
  description = <<-DOCUMENTATION
A list of options to apply to the Option Group:
  - option_name                    (required, string):       Name of the option setting (e.g. MEMCACHED).
  - port                           (optional, number):       Port number of connection to specific option setting - like MEMCACHED. (e.g. 11211).
  - version                        (optional, string):       Version of the option setting (e.g. 13.1.0.0).
  - db_security_group_memberships  (optional, list(string)): List of DB Security Groups for which the option is enabled.
  - vpc_security_group_memberships (optional, list(string)): List of VPC Security Groups for which the option is enabled.
  - option_settings                (required, list(map)):    List of map of option settings to apply:
    - name  (required, string): Name of the setting.
    - value (required, string): Value of the setting.
DOCUMENTATION
  type = list(object({
    option_name                    = string
    port                           = optional(number)
    version                        = optional(string)
    db_security_group_memberships  = optional(list(string))
    vpc_security_group_memberships = optional(list(string))
    option_settings = list(object({
      name  = string
      value = string
    }))
  }))
  default  = []
  nullable = false

  validation {
    condition     = length(var.option_group_options) == 0 || var.option_group != null
    error_message = "“var.option_group” needs to be set when “var.option_group_options” are given."
  }
  validation {
    condition     = length(var.option_group_options) == 0 || !var.database.cluster
    error_message = "“var.option_group_options” is not compatible with a cluster-type database."
  }
}

#####
# Resources
#####

locals {
  db_option_group_needed = !var.database.cluster && var.option_group != null
}

resource "aws_db_option_group" "this" {
  for_each = local.db_option_group_needed ? { 0 = "enabled" } : {}

  name                     = "${var.testing_prefix}${var.option_group.name}"
  engine_name              = var.option_group.engine_name
  major_engine_version     = var.option_group.major_engine_version
  option_group_description = var.option_group.description != null ? var.option_group.description : local.default_description
  skip_destroy             = var.option_group.skip_destroy

  dynamic "option" {
    for_each = var.option_group_options

    content {
      option_name                    = option.value.option_name
      port                           = lookup(option.value, "port", null)
      version                        = lookup(option.value, "version", null)
      db_security_group_memberships  = lookup(option.value, "db_security_group_memberships", null)
      vpc_security_group_memberships = lookup(option.value, "vpc_security_group_memberships", null)

      dynamic "option_settings" {
        for_each = option.value.option_settings

        content {
          name  = option_settings.value.name
          value = option_settings.value.value
        }
      }
    }
  }

  tags = merge(
    local.tags,
    var.option_group.tags,
    {
      Name        = "${var.testing_prefix}${var.option_group.name}"
      Description = var.option_group.description != null ? var.option_group.description : local.default_description
    },
  )
}

#####
# Outputs
#####

output "aws_db_option_group" {
  value = local.db_option_group_needed ? { for k, v in aws_db_option_group.this["0"] :
    k => v if !contains(["tags", "option"], k)
  } : null
}
